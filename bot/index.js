const sendData = async () => {
  //generate random number from 1 to 50
  const random = Math.floor(Math.random() * 50) + 1;
  //unix timestamp now
  const timestamp = Date.now();
  const newdata = { id: timestamp, value: random };
  try {
    // console.log(newdata);
    const response = await fetch("http://localhost:5000/api/saham", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newdata),
    });
    if (response.ok) {
      console.log("Data sent successfully");
    }
    const data = await response.json();
    console.log(data);
  } catch (error) {
    console.error("Error:", error.message);
  }
};

setInterval(() => {
  sendData();
}, 5000);
