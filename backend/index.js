import express from "express";
import cors from "cors";

const app = express();

app.use(express.json());
app.use(
  cors({
    origin: "http://127.0.0.1:5500",
  })
);

//untuk menyimpan koneksi client sse yang aktif
const clients = [];
// database array saham sederhana
const saham = [];

app.post("/api/saham", (req, res) => {
  const { id, value } = req.body;
  saham.push({ id, value });
  //kirim pembaruan ke client
  sendSSEUpdate(saham);
  res.json({ id, value });
});

//server sent event endpoint
app.get("/api/saham", (req, res) => {
  //set header untuk server sent event
  res.setHeader("Content-Type", "text/event-stream");
  res.setHeader("Cache-Control", "no-cache");
  res.setHeader("Connection", "keep-alive");

  //ketika client terhubung, tambahkan res ke daftar client
  const client = {
    id: Date.now(),
    res,
  };
  clients.push(client);

  //ketika client disconnect, hapus client dari daftar
  req.on("close", () => {
    console.log(`${client.id} Connection closed`);
    clients.splice(clients.indexOf(client), 1);
    res.end();
  });
});

//kirim pembaruan ke client
function sendSSEUpdate(data) {
  clients.forEach((client) => {
    //kirim data ke client
    const responseData = `data: ${JSON.stringify(data)}\n\n`;
    client.res.write(responseData);
  });
}

app.listen(5000, () => {
  console.log("Server is running on port 5000");
});
